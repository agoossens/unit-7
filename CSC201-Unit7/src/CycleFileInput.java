import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.PrintWriter;
import java.util.Scanner;


public class CycleFileInput 
{
	/**
	 * Algorithm to solve the problem
	 * 1) Create the different variables to define wheels and weight
	 * 2) Create a predefined constructor 
	 * 3) Assign the values to wheels and weight, 100 & 1000
	 * 4) create a to string method
	 * 5) create a demo class to print out the beginning and changed result
	 */
	private double numberOfWheels;
	private double weight;
	
	public CycleFileInput()
	{
		readFileInformation();
	}
	

	public void SaveCycleFileInput() 
	{
		String fileName = "CycleOutputforSecond.txt";
		PrintWriter outputStream = null; 
		
		try
		{
			outputStream = new PrintWriter(fileName);
		}
		catch (Exception a)
		{
			System.out.println("Error opening file " + fileName);
			System.exit(0);
		}
		
		outputStream.println(numberOfWheels);
		outputStream.println(weight);
		outputStream.close();
		System.out.println("The code was written to " + fileName );
	}

	public void readFileInformation()
	{
		String fileName = "CycleOutInputReader.txt";
		PrintWriter outputStream = null; 
		
		try
		{
			outputStream = new PrintWriter(fileName);
		}
		catch (Exception a)
		{
			System.out.println("Error opening file " + fileName);
			System.exit(0);
		}
		outputStream.println("500");
		outputStream.println("300");
		outputStream.close();
		

		try
		{
			BufferedReader br = new BufferedReader(new FileReader("CycleOutInputReader.txt"));
			numberOfWheels = Double.parseDouble(br.readLine());
			weight = Double.parseDouble(br.readLine());
			br.close();
			System.out.println("Successfully read from file.");
		}
		catch (Exception io)
		{
			io.printStackTrace();
		}
	}

	
	
	public double getNumberOfWheels() {
		return numberOfWheels;
	}

	public void setNumberOfWheels(double numberOfWheels) {
		this.numberOfWheels = numberOfWheels;
	}

	public double getWeight() {
		return weight;
	}

	public void setWeight(double weight) {
		this.weight = weight;
	}
	

	public CycleFileInput (int numberOfWheels, int weight)
	{
		this.numberOfWheels = numberOfWheels;
		this.weight = weight; 
	}

	public String toString()
	{
		return (""+numberOfWheels+","+weight);
	}
}
