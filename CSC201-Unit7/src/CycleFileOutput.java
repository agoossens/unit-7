import java.io.PrintWriter;
import java.util.Scanner;


public class CycleFileOutput 
{
	/**
	 * Algorithm to solve the problem
	 * 1) Create the different variables to define wheels and weight
	 * 2) Create a predefined constructor 
	 * 3) Assign the values to wheels and weight, 100 & 1000
	 * 4) create a to string method
	 * 5) create a demo class to print out the beginning and changed result
	 */
	private double numberOfWheels;
	private double weight;
	
	

	public void SaveCycleFileOutput() 
	{
		String fileName = "CycleOut1.txt";
		PrintWriter outputStream = null; 
		
		try
		{
			outputStream = new PrintWriter(fileName);
		}
		catch (Exception a)
		{
			System.out.println("Error opening file " + fileName);
			System.exit(0);
		}
		
		outputStream.println(numberOfWheels);
		outputStream.println(weight);
		outputStream.close();
		System.out.println("The code was written to " + fileName );
	}

	public CycleFileOutput() //will be changed to these values
	{
		numberOfWheels = 100;
		weight = 1000;
	}

	public CycleFileOutput (int numberOfWheels, int weight)
	{
		this.numberOfWheels = numberOfWheels;
		this.weight = weight; 
	}

	public String toString()
	{
		return (""+numberOfWheels+","+weight);
	}
}
