import java.util.Scanner;


public class CycleThrowCatch 
{
	/**
	 * Algorithm to solve the problem
	 * 1) Create the different variables to define wheels and weight
	 * 2) Create a predefined constructor 
	 * 3) Assign the values to wheels and weight, 100 & 1000
	 * 4) create a to string method
	 * 5) create a demo class to print out the beginning and changed result
	 */
	private double numberOfWheels;
	private double weight;
	
	public CycleThrowCatch() throws Exception
	{
		Scanner keyboard = new Scanner(System.in);
		try
		{
			System.out.println("Please enter the weight");
			weight = keyboard.nextDouble();
			System.out.println("Please enter the number of wheels.");
			numberOfWheels = keyboard.nextDouble();
		}
		catch (Exception a)
		{
			System.out.println("the values must be entered as doubles");
			System.exit(0);
		}
		if(weight <=0 || numberOfWheels <= 0)
		{
			throw new Exception("values need to be greater than zero.");
		}
	}

	
	
	
	public void CycleThrowCatch () //will be changed to these values
	{
		numberOfWheels = 100;
		weight = 1000;
	}
	
	public CycleThrowCatch  (double numberOfWheels, double weight)
	{
		this.numberOfWheels = numberOfWheels;
		this.weight = weight; 
	}

	public String toString()
	{
		return (""+numberOfWheels+","+weight);
	}
}
